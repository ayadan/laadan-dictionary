#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

infile = open( "laadan-to-english_2018-06-21.json", "r" )

laadanDict = json.loads( infile.read() )

outfile = open( "laadan.tex", "w" )

i = 0
outfile.write( "\\section*{Láadan to English Dictionary} \n" )
outfile.write( "\\begin{tabular}{p{3cm} p{9cm}} \n" )
for entry in laadanDict:
    entry["láadan"]         = str(entry["láadan"]).replace( "&", "\\&" );
    entry["description"]    = str(entry["description"]).replace( "&", "\\&" );
    entry["english"]        = str(entry["english"]).replace( "&", "\\&" );
    entry["english"]        = str(entry["english"]).replace( "", "" );
    entry["english"]        = str(entry["english"]).replace( "", "" );
    entry["classification"] = str(entry["classification"]).replace( "&", "\\&" );
    entry["word breakdown"] = str(entry["word breakdown"]).replace( "&", "\\&" );
    entry["notes"]          = str(entry["notes"]).replace( "&", "\\&" );
    
    outfile.write( entry["láadan"] + " & " + entry["english"] + " \\\\ \n" )
    
    #outfile.write( "\paragraph{" + entry["láadan"] + "} \\tab " + entry["english"] + " ~\\\\ \n" )
    #outfile.write( "\\tab " + str( entry["description"] ) + " ~\\\\ \n" )
    #outfile.write( "\\tab " + str( entry["classification"] ) + " ~\\\\ \n" )
    
    #if ( entry["word breakdown"] != "" ):
    #    outfile.write( "\\tab " + str( entry["word breakdown"] ) + " ~\\\\ \n" )
        
    #if ( entry["notes"] != "" ):
    #    outfile.write( "\\tab " + str( entry["notes"] ) + " ~\\\\ \n" )

    #outfile.write( "\n" )
    
    if i % 15 == 0 and i != 0:
        outfile.write( "\\end{tabular} \n" )
        outfile.write( "\\begin{tabular}{p{3cm} p{9cm}} \n" )
        
    i += 1
        
outfile.write( "\\end{tabular} \n" )
